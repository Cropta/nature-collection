package fr.Cropt4.naturecollection.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.Cropt4.naturecollection.MainActivity
import fr.Cropt4.naturecollection.PlantRepository.Singleton.plantList
import fr.Cropt4.naturecollection.R
import fr.Cropt4.naturecollection.adapteur.PlantAdapteur
import fr.Cropt4.naturecollection.adapteur.PlantItemDecoration

class CollectionFragment(
    private val context: MainActivity
): Fragment(){

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =inflater?.inflate(R.layout.fragment_collection,container,false)
        //recuperer la recyclerView
        val collectionRecyclerView = view.findViewById<RecyclerView>(R.id.collection_recycler_list)

        collectionRecyclerView.adapter = PlantAdapteur(context , plantList.filter { it.liked }, R.layout.item_vertical_plant )
        collectionRecyclerView.layoutManager= LinearLayoutManager(context)
        collectionRecyclerView.addItemDecoration(PlantItemDecoration())


        return view

    }
}