package fr.Cropt4.naturecollection.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import fr.Cropt4.naturecollection.MainActivity
import fr.Cropt4.naturecollection.PlantModel
import fr.Cropt4.naturecollection.PlantRepository.Singleton.plantList
import fr.Cropt4.naturecollection.R
import fr.Cropt4.naturecollection.adapteur.PlantAdapteur
import fr.Cropt4.naturecollection.adapteur.PlantItemDecoration

//Homefragment : Fragment est un heritage pour comprendre Homefragment comme etant lui meme un fragment de androidx
class Homefragment (private val context:MainActivity) : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,  savedInstanceState: Bundle?): View? {

        //inflater permet de pourvoir injecter sur home fragment le layout associé -->inflater? (parce que le layout peut ne pas etre trouver) et .inflate pour injecter le layout
        val view = inflater?.inflate(R.layout.fragment_home,container, false)





        //recuperer le recyclerview Horizontal

        val horizontalRecyclerView = view.findViewById<RecyclerView>(R.id.horizontal_recycler_view)
        //Adaptation du de la vue  grace a la classe PlantAdapteur
        horizontalRecyclerView.adapter = PlantAdapteur(context, plantList.filter { !it.liked } , R.layout.item_horizontal_plant)

        //recuperer le recyclerview Vertical

        val verticalRecyclerView = view.findViewById<RecyclerView>(R.id.vertical_recycler_view)
        verticalRecyclerView.adapter = PlantAdapteur(context ,plantList ,R.layout.item_vertical_plant)

        //ajout de la marge vers le bas entre chaque item
        verticalRecyclerView.addItemDecoration(PlantItemDecoration())

        return view
    }
}