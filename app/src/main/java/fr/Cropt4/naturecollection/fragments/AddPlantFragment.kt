package fr.Cropt4.naturecollection.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Spinner
import androidx.fragment.app.Fragment
import fr.Cropt4.naturecollection.MainActivity
import fr.Cropt4.naturecollection.PlantModel
import fr.Cropt4.naturecollection.PlantRepository
import fr.Cropt4.naturecollection.PlantRepository.Singleton.downloadUri
import fr.Cropt4.naturecollection.R
import java.util.*

class AddPlantFragment (
        private val context:MainActivity
        ):Fragment(){

        private var file: Uri? =null
        private var uploadedImage:ImageView?=null

        override fun onCreateView(
                inflater: LayoutInflater,
                container: ViewGroup?,
                savedInstanceState: Bundle?
        ): View? {
                val view =inflater?.inflate(R.layout.fragment_add_plant,container,false)

                //recuperer uploadedImage pour lui associer son composant
                uploadedImage=view.findViewById(R.id.preview_image)

                //recuperer le button pour charger l'image
                val pickupImageButton= view.findViewById<Button>(R.id.upload_button)
                //Ouverture de l'image du télephone lors d'un clique
                pickupImageButton.setOnClickListener{pickupImage()}

                //recuperer le boutton de confirmation
                val confirmButton = view.findViewById<Button>(R.id.confirm_button)

                confirmButton.setOnClickListener{sendForm(view)}

                return view
        }

        private fun sendForm(view: View) {
                val repo= PlantRepository()
                repo.uploadImage(file!!){

                        val plantName= view.findViewById<EditText>(R.id.name_input).text.toString()
                        val plantDescription = view.findViewById<EditText>(R.id.description_input).text.toString()
                        val  grow = view.findViewById<Spinner>(R.id.grow_spinner).selectedItem.toString()
                        val water = view.findViewById<Spinner>(R.id.water_spinner).selectedItem.toString()
                        val downloadImageUrl=downloadUri

                        //crée un nouvelle objet PlantModel
                        val test=PlantModel(
                                UUID.randomUUID().toString(),
                                plantName,
                                plantDescription,
                                downloadImageUrl.toString(),grow, water

                        )
                        /*val plant = PlantModel(
                                UUID.randomUUID().toString(),
                                plantName,
                                plantDescription,
                                downloadImageUrl.toString(),
                                grow,
                                water
                        )*/
                        //envoyer en bdd
                        repo.insertPlant(test)

                }





        }

        /*Celui ci  fonctionne  meme avec starActivityForResult Obscelete*/
        //Permet d'accéder aux element contenue dans notre appareil grace a Intent() le choix de l'element est de type image
        private fun pickupImage() {
                val intent = Intent()
                intent.type="image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent,"Select Picture"),47)
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
                super.onActivityResult(requestCode, resultCode, data)
                if (requestCode== 47 && resultCode== Activity.RESULT_OK){

                        if(data == null|| data.data== null )return

                        file =data.data

                        uploadedImage?.setImageURI(file)





                }
        }






}