package fr.Cropt4.naturecollection

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.Cropt4.naturecollection.fragments.AddPlantFragment
import fr.Cropt4.naturecollection.fragments.CollectionFragment
import fr.Cropt4.naturecollection.fragments.Homefragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadFragment(Homefragment(this),R.string.home_page_title )

        //importer la bottomnavigationvie

        val navigationView = findViewById<BottomNavigationView>(R.id.navigation_view)

        navigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.home_page->{
                    loadFragment(Homefragment(this),R.string.home_page_title)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.collection_page->{
                    loadFragment(CollectionFragment(this),R.string.collection_page_title)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.add_plant_page->{
                    loadFragment(AddPlantFragment(this),R.string.add_plant_page_title)
                    return@setOnNavigationItemSelectedListener true
                }
                else -> false
            }
        }



    }

    private fun loadFragment(fragment: Fragment,string: Int) {
        //charger notre repository
        val repo = PlantRepository()


        //actualiser le titre de la page
        findViewById<TextView>(R.id.page_title).text = resources.getString(string)

        //mettre a jour la liste de plantes
        repo.updateData {
            //injecter le fragment dans notre boite(fragment container)
            //supportFragmentManager permet de gerer les fragment sur android   beginTransaction commence une serie d'opération a fin de pourvoir manipuler les fragments

            val transaction =supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, fragment)
            //transaction.addToBackStack  évite d'avoir un retour sur ce composant
            transaction.addToBackStack(null)

            //transaction.commit pour envoyer le changement
            transaction.commit()


        }


    }
}