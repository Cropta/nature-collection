package fr.Cropt4.naturecollection


//Class PlantModel nous Permettra de listé toute les caractéristique d'une plante

class PlantModel(

    val id : String = "plant0",
    val name: String ="Tulipe",
    val description:String = "petite description",
    val imageUrl : String = "https://www.fightersgeneration.com/nz4/char/skullomania-sfex2-perfect-animation.gif",
    val grow: String ="Faible",
    val water:String ="Moyenne",
    var liked : Boolean = false


)