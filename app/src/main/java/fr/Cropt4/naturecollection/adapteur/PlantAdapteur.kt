package fr.Cropt4.naturecollection.adapteur

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.Cropt4.naturecollection.*

class PlantAdapteur(
    val context:MainActivity,
    private val plantList : List<PlantModel> ,
    private val layoutId: Int
    ) :RecyclerView.Adapter<PlantAdapteur.ViewHolder>(){

    //boite pour ranger tous les composant a controlé
    class ViewHolder(view: View):RecyclerView.ViewHolder(view) {

        val plantImage= view.findViewById<ImageView>(R.id.image_item)
        val plantName:TextView?= view.findViewById(R.id.name_item)
        val plantDescription:TextView?= view.findViewById(R.id.descrition_item)
        val starIcon = view.findViewById<ImageView>(R.id.star_icon)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(layoutId, parent ,false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //recuperer les informations de la plante
        val currentPlant =plantList[position]

        //recuperer le repository
        val repo = PlantRepository()

        //utilisation de glide pour recuperer l'image a partir de son lien ->composant
        Glide.with(context).load(Uri.parse(currentPlant.imageUrl)).into(holder.plantImage)

        //mettre a jour le nom de la plante
        holder.plantName?.text = currentPlant.name

        //mettre a jour la description de la plante
        holder.plantDescription?.text= currentPlant.description

        //vérification si la plante a été liké

        if(currentPlant.liked){
            holder.starIcon.setImageResource(R.drawable.ic_star)
        }else{

            holder.starIcon.setImageResource(R.drawable.ic_unstar)
        }

        //Rajout d'une  interaction sur l'etoile
        holder.starIcon.setOnClickListener{
            //inverse si le boutton est like ou non
            currentPlant.liked = !currentPlant.liked
            //mettre ajout l'objet plant
            repo.updatePlant(currentPlant)
        }

        //interaction lors du clic sur une plante
        holder.itemView.setOnClickListener {
            //afficher la pop
            PlantPopup(this, currentPlant).show()
        }


    }

    override fun getItemCount(): Int = plantList.size
}