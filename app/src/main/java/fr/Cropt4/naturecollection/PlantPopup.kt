package fr.Cropt4.naturecollection

import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import fr.Cropt4.naturecollection.adapteur.PlantAdapteur

class PlantPopup(
    private val adapter : PlantAdapteur,
    private val currentPlant: PlantModel

) :Dialog(adapter.context){
//OnCreate pour l'injection du layout popup deja crée
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Pour dire que l'on ne veux pas de titre sur notre fenetre
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        //choix du layout a injecter
        setContentView(R.layout.popup_plants_details)

        setupComponents()
        setupCloseButton()
        setupDeleteButton()
        setupStarButton()
    }
    private fun updateStar(button: ImageView){
        if (currentPlant.liked){
            button.setImageResource(R.drawable.ic_star)
        }else{
            button.setImageResource(R.drawable.ic_unstar)}
    }

    private fun setupStarButton() {
        //recuperer
        val starButton = findViewById<ImageView>(R.id.star_button)

        updateStar(starButton)

        //interaction
        starButton.setOnClickListener{
            currentPlant.liked= !currentPlant.liked
            val repo = PlantRepository()
            repo.updatePlant(currentPlant)
            updateStar(starButton)
        }
    }

    private fun setupDeleteButton() {
        findViewById<ImageView>(R.id.delete_button).setOnClickListener{
            //de supprimer la plante de la base de donnée
            val repo = PlantRepository()
            repo.deletePlant(currentPlant)
            dismiss()
        }
    }

    private fun setupCloseButton() {
        findViewById<ImageView>(R.id.close_button).setOnClickListener{
            //fermer la fenetre avec dismiss qui permet de fermer le composant actuel
            dismiss()
        }
    }

    private fun setupComponents() {
        //actualiser l'image de plant
        val plantImage = findViewById<ImageView>(R.id.image_item)
        //actualiser l'image
        Glide.with(adapter.context).load(Uri.parse(currentPlant.imageUrl)).into(plantImage)

        //actualiser le nom de la plante
        findViewById<TextView>(R.id.popup_plant_name).text = currentPlant.name

        //actualiser la description de la plante
        findViewById<TextView>(R.id.popup_plant_description_subtitle).text = currentPlant.description

        //actualiser La Croissance de la plante
        findViewById<TextView>(R.id.popup_plant_grow_subtitle).text = currentPlant.grow

        //actualiser la consommaton en eau de la plante

        findViewById<TextView>(R.id.popup_plant_water_subtitle).text = currentPlant.water
    }
}