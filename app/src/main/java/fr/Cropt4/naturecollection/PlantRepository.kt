package fr.Cropt4.naturecollection

import android.net.Uri
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import fr.Cropt4.naturecollection.PlantRepository.Singleton.databaseRef
import fr.Cropt4.naturecollection.PlantRepository.Singleton.downloadUri
import fr.Cropt4.naturecollection.PlantRepository.Singleton.plantList
import fr.Cropt4.naturecollection.PlantRepository.Singleton.storageReference
import java.net.URI
import java.util.*



class PlantRepository {


    //pour eviter le rechargement a chaque fois que l'on change de page dans  l'application
    object Singleton{

        //donner le lien du Bucket
        private val BUCKET_URL:String= "gs://nature-collection-22aea.appspot.com"

        //se connecter a notre espace de stockage
        val storageReference=FirebaseStorage.getInstance().getReferenceFromUrl(BUCKET_URL)

        //se Connecter a la reference plant
        val databaseRef = FirebaseDatabase.getInstance().getReference("plants")

        //creer une liste qui va contenir nos plantes
        val  plantList = arrayListOf<PlantModel>()

        //contenir le lien de l'image courante
        var downloadUri :Uri? = null
    }

    fun updateData(callback: ()->Unit){

        // absorber les données depuis la databaseRef -> liste de plante
        databaseRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {

                //retirer les anciennes  plant
                plantList.clear()

                //Recolter la liste
                for (ds in snapshot.children){

                    //construire un objet plante (le transformé en plant model)
                    val plant = ds.getValue(PlantModel :: class.java)

                    //verifier que la plante n'est pas null
                    if (plant !=null){

                        plantList.add(plant)
                    }
                }
                //actionner le callback
                callback()

            }

            override fun onCancelled(error: DatabaseError) {
            }

        })
    }

    //creer une fonction pour envoyer les fichiers sur le storage
    fun uploadImage(file: Uri, callback: () -> Unit){

        if(file!=null){

            //fileName pour cree un nom aleatoire
            val fileName= UUID.randomUUID().toString()+".jpg"

            //pour donner le chemin du lieu d'enregistrement
            val ref = storageReference.child(fileName)

            //contenue a soumettre
            val  uploadTask = ref.putFile(file)

            //demarrer la tache d'envoi
            uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot,Task<Uri>>{task->

                //si il y a des probleme lors de l'envoie
                if (!task.isSuccessful){
                    task.exception?.let { throw it }
                }
                return@Continuation ref.downloadUrl

            }).addOnCompleteListener{
                task ->

                //vérifier si tout a bien fonctionnée
                if (task.isSuccessful){

                    //recuperer l'image
                    downloadUri = task.result
                    callback()
                }

            }
        }

    }

    //mettre ajout l'objet plante en bdd
    fun updatePlant(plant:PlantModel){
        databaseRef.child(plant.id).setValue(plant)}

    //insere une nouvelle plante dans la bdd
    fun insertPlant(plant:PlantModel)= databaseRef.child(plant.id).setValue(plant)


    //supprimer une plant de la base
    fun deletePlant(plant:PlantModel)= databaseRef.child(plant.id).removeValue()
}






